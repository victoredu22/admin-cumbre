
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.toastr = require('toastr');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('biblioteca', require('./components/BibliotecaComponent.vue'));

Vue.component('example-component', require('./components/ExampleComponent.vue'));

Vue.component('edit-libro', require('./components/EditLibroComponent.vue'));
Vue.component('create-libro', require('./components/CreateLibroComponent.vue'));
Vue.component('delete-libro', require('./components/DeleteLibroComponent.vue'));


Vue.component('panel-libro', require('./components/panelLibros/listaLibrosComponent.vue'));


const app = new Vue({
    el: '#app'
});
