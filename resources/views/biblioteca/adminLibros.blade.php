@extends('layout')
@section('content')

<main id="app">
		<biblioteca></biblioteca>           
		<edit-libro></edit-libro>
		<create-libro></create-libro>
		<delete-libro></delete-libro>
</main>
@endsection