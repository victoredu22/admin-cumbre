-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 16-11-2018 a las 03:25:21
-- Versión del servidor: 5.7.21
-- Versión de PHP: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cumbre`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblalumnoxcurso`
--

DROP TABLE IF EXISTS `tblalumnoxcurso`;
CREATE TABLE IF NOT EXISTS `tblalumnoxcurso` (
  `idAlumnoxcurso` int(11) NOT NULL AUTO_INCREMENT,
  `idCurso` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idAlumnoxcurso`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblalumnoxcurso`
--

INSERT INTO `tblalumnoxcurso` (`idAlumnoxcurso`, `idCurso`, `idUsuario`) VALUES
(1, 1, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblasignatura`
--

DROP TABLE IF EXISTS `tblasignatura`;
CREATE TABLE IF NOT EXISTS `tblasignatura` (
  `idAsignatura` int(11) NOT NULL AUTO_INCREMENT,
  `nombreAsignatura` varchar(250) NOT NULL,
  PRIMARY KEY (`idAsignatura`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblasignatura`
--

INSERT INTO `tblasignatura` (`idAsignatura`, `nombreAsignatura`) VALUES
(1, 'lenguaje'),
(2, 'matematica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcurso`
--

DROP TABLE IF EXISTS `tblcurso`;
CREATE TABLE IF NOT EXISTS `tblcurso` (
  `idCurso` int(11) NOT NULL AUTO_INCREMENT,
  `nombreCurso` varchar(250) NOT NULL,
  PRIMARY KEY (`idCurso`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblcurso`
--

INSERT INTO `tblcurso` (`idCurso`, `nombreCurso`) VALUES
(1, 'primero'),
(2, 'segundo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbldetalleusuario`
--

DROP TABLE IF EXISTS `tbldetalleusuario`;
CREATE TABLE IF NOT EXISTS `tbldetalleusuario` (
  `idDetalleUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  PRIMARY KEY (`idDetalleUsuario`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbldetalleusuario`
--

INSERT INTO `tbldetalleusuario` (`idDetalleUsuario`, `idUsuario`, `nombre`) VALUES
(1, 1, 'victor'),
(2, 2, 'jorge');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbllibro`
--

DROP TABLE IF EXISTS `tbllibro`;
CREATE TABLE IF NOT EXISTS `tbllibro` (
  `idLibro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `autor` varchar(100) NOT NULL,
  `detalle` varchar(100) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`idLibro`),
  KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbllibro`
--

INSERT INTO `tbllibro` (`idLibro`, `nombre`, `autor`, `detalle`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'sdad', 'dsa', 'dsa', 0, NULL, NULL),
(2, 'dsadsa', 'dsa', 'dsa', 0, NULL, NULL),
(3, 'sdadsa', 'sads', 'dsa', 0, NULL, NULL),
(4, '1231', '3121', '231', 0, NULL, NULL),
(5, 'dsfsd', 'fds', 'fds', 0, NULL, NULL),
(6, 'principito', 'pepito', 'es muy buen libro', 1, NULL, NULL),
(7, 'papelucho', 'gabriela mistral', 'libro buenisimo', 1, NULL, NULL),
(8, 'biblia', 'jebus', 'libro aburrido', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbllibrostock`
--

DROP TABLE IF EXISTS `tbllibrostock`;
CREATE TABLE IF NOT EXISTS `tbllibrostock` (
  `idStock` int(100) NOT NULL AUTO_INCREMENT,
  `idLibro` int(100) NOT NULL,
  `cantidad` int(100) NOT NULL,
  PRIMARY KEY (`idStock`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbllibrostock`
--

INSERT INTO `tbllibrostock` (`idStock`, `idLibro`, `cantidad`) VALUES
(1, 1, 23),
(2, 2, 321),
(3, 3, 231),
(4, 4, 321),
(5, 5, 34),
(6, 6, 2),
(7, 7, 3),
(8, 8, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblprestamo`
--

DROP TABLE IF EXISTS `tblprestamo`;
CREATE TABLE IF NOT EXISTS `tblprestamo` (
  `idPedido` int(100) NOT NULL AUTO_INCREMENT,
  `idAlumno` int(11) NOT NULL,
  `idAsignador` int(11) NOT NULL,
  `idLibro` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idPedido`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblprestamo`
--

INSERT INTO `tblprestamo` (`idPedido`, `idAlumno`, `idAsignador`, `idLibro`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, '2018-07-29 23:30:07', NULL),
(2, 1, 2, 1, '2018-07-29 23:30:44', NULL),
(3, 1, 2, 1, '2018-07-29 23:31:34', NULL),
(4, 1, 2, 1, '2018-07-29 23:31:55', NULL),
(5, 1, 2, 1, '2018-07-29 23:32:12', NULL),
(6, 1, 2, 2, '2018-07-31 04:07:44', NULL),
(7, 1, 2, 2, '2018-07-31 04:08:39', NULL),
(8, 1, 2, 2, '2018-07-31 04:09:27', NULL),
(9, 1, 2, 2, '2018-07-31 04:11:22', NULL),
(10, 2, 2, 2, '2018-08-03 01:42:37', NULL),
(11, 1, 2, 1, '2018-08-03 02:38:41', NULL),
(12, 1, 2, 1, '2018-08-03 03:00:37', NULL),
(13, 2, 2, 1, '2018-08-03 03:01:01', NULL),
(14, 1, 2, 1, '2018-08-03 03:02:14', NULL),
(15, 1, 2, 2, '2018-08-03 03:02:34', NULL),
(16, 1, 2, 2, '2018-08-03 03:04:18', NULL),
(17, 1, 2, 2, '2018-08-03 03:04:40', NULL),
(18, 2, 2, 2, '2018-08-03 03:08:06', NULL),
(19, 1, 2, 2, '2018-08-03 03:08:30', NULL),
(20, 1, 2, 1, '2018-08-05 18:27:39', NULL),
(21, 1, 2, 2, '2018-08-05 18:29:39', NULL),
(22, 1, 2, 2, '2018-08-05 18:30:41', NULL),
(23, 1, 2, 2, '2018-08-05 18:37:47', NULL),
(24, 1, 2, 2, '2018-08-05 18:40:35', NULL),
(25, 1, 2, 3, '2018-08-05 18:40:52', NULL),
(26, 1, 2, 1, '2018-08-05 18:42:33', NULL),
(27, 1, 2, 2, '2018-08-05 18:42:53', NULL),
(28, 2, 2, 1, '2018-08-05 18:43:03', NULL),
(29, 1, 2, 1, '2018-08-05 18:43:10', NULL),
(30, 1, 2, 3, '2018-08-05 18:43:15', NULL),
(31, 1, 2, 1, '2018-08-05 18:43:55', NULL),
(32, 1, 2, 1, '2018-08-05 18:43:59', NULL),
(33, 1, 2, 1, '2018-08-05 18:44:03', NULL),
(34, 1, 2, 2, '2018-08-05 18:44:08', NULL),
(35, 1, 2, 2, '2018-08-05 18:44:13', NULL),
(36, 1, 2, 1, '2018-08-05 19:29:57', NULL),
(37, 1, 2, 1, '2018-08-05 19:44:26', NULL),
(38, 1, 2, 2, '2018-08-05 19:49:08', NULL),
(39, 2, 2, 2, '2018-08-05 19:49:14', NULL),
(40, 1, 2, 1, '2018-08-05 22:31:40', NULL),
(41, 1, 2, 8, '2018-11-15 04:05:39', NULL),
(42, 2, 2, 8, '2018-11-15 04:05:58', NULL),
(43, 1, 2, 8, '2018-11-15 04:06:31', NULL),
(44, 1, 2, 8, '2018-11-15 04:14:33', NULL),
(45, 1, 2, 8, '2018-11-15 04:16:47', NULL),
(46, 1, 2, 8, '2018-11-15 04:18:19', NULL),
(47, 1, 2, 8, '2018-11-15 04:18:53', NULL),
(48, 1, 2, 8, '2018-11-15 04:19:41', NULL),
(49, 1, 2, 8, '2018-11-15 04:20:03', NULL),
(50, 1, 2, 6, '2018-11-15 04:21:20', NULL),
(51, 1, 2, 7, '2018-11-15 04:21:53', NULL),
(52, 1, 2, 7, '2018-11-15 04:39:00', NULL),
(53, 1, 2, 7, '2018-11-16 01:30:19', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblusuario`
--

DROP TABLE IF EXISTS `tblusuario`;
CREATE TABLE IF NOT EXISTS `tblusuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `numeroDocumento` int(11) NOT NULL,
  `email` varchar(250) NOT NULL,
  `passwod` varchar(250) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblusuario`
--

INSERT INTO `tblusuario` (`idUsuario`, `numeroDocumento`, `email`, `passwod`) VALUES
(1, 17708, 'victor', '123'),
(2, 17608, 'jorge', '234');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
