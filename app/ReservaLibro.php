<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ReservaLibro extends Model
{
   	/**
	 * Método que inserta una nueva reserva
	 *
	 * @param $request
	 * @return array => stdArray
	 * @author Victor Curilao
	 **/
  	public function createReserva($request){

  		$insert = DB::table('tblprestamo')
  			->insertGetId(array(
  					'idAlumno'=> $request->idAlumno,
  					'idAsignador'=>2,
  					'idLibro'=>$request->idLibro,
  			));

	return $insert; 
  }
}
