<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BibliotecaController extends Controller
{
    
	 /**
     * Método que obtiene la lista total de libros
     *
     * @param $request
     * @return array => stdArray
     * @author Victor Curilao
     **/ 
    public function adminLibros(){


        return view("biblioteca.adminLibros");
    }


     /**
     * Método que muestra la vista de los libros
     *
     * @param $request
     * @return array => stdArray
     * @author Victor Curilao
     **/ 
    public function librosIndex(){

        return view("biblioteca.panelLibros.indexLibros");
    }

}
