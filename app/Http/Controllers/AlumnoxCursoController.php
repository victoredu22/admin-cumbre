<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AlumnoxCurso;


class AlumnoxCursoController extends Controller
{
     /**
	 * Método obtiene el curso segun el id.
	 *
	 * @param $idCurso
	 * @return array => stdArray
	 * @author Victor Curilao
	 **/
    public function getCursoById($idCurso){

    	$objAlumnosxCurso = new AlumnoxCurso();
    	$AlumnoxCursos = $objAlumnosxCurso->getCursoById($idCurso);

    	return \Response::json($AlumnoxCursos);

    }
}
