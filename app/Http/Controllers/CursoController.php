<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use App\ReservaLibro;
use App\StockLibro;

class CursoController extends Controller
{
    
    /**
	 * Método que obtiene una lista de cursos, a travez de json
	 *
	 * @return array => stdArray
	 * @author Victor Curilao
	 **/
    public function getCursos(){

	   	$objCursos = new Curso();
	   	$cursos = $objCursos->getAllCursos();

   	 return \Response::json($cursos);
   }

   	/**
	 * Método que inserta una nueva reserva, en la biblioteca
	 *
	 * @param $request
	 * @return array => stdArray
	 * @author Victor Curilao
	 **/
	public function insertReserva(Request $Request){


		$rules = [
			"idLibro" => "required",
			"idCurso" => "required",
			"idAlumno"=>"required"
		];

		$msgValidate = [
			"required" => "Debes seleccionar al menos una opcion.",
			"min" => "Este campo admite al menos :min caracteres.",
			"max" => "Este campo admite como máximo :max caracteres."
		];

		$validador = $this->validate($Request, $rules, $msgValidate);


		if(!empty($validador) == true ){

			$objCursos = new ReservaLibro();
			$nuevoPrestamo = $objCursos->createReserva($Request);
		
			$objStock = new StockLibro();
			$estadoStock = $objStock->updateStock($Request->idLibro);

			
		}
			
   }	
}
