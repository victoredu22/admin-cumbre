<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class StockLibro extends Model
{
     /**
	 * Método actualiza el stock de los libros
	 *
	 * @param $idLibro
	 * @return updateStock
	 * @author Victor Curilao
	 **/
    public function updateStock($idLibro){

    	$updateStock = DB::table('tbllibrostock')
    				    ->where('idLibro','=',$idLibro)
    				    ->decrement('cantidad',1);
    
        return $updateStock;
    }
}
