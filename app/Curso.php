<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Curso extends Model
{
   	/**
     * Método que busca la lista de libros actual.
     *
     * @param $request
     * @return array => stdArray
     * @author Victor Curilao
     **/  
    public function getAllCursos(){
    	
    	$get = DB::table('tblcurso')
    		->get()
    		->all();

    	return $get;

    }
}
