<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Libro extends Model
{	

	 /**
     * Método que busca la lista de libros actual.
     *
     * @param $request
     * @return array => stdArray
     * @author Victor Curilao
     **/  
    public function getLibrosAll(){

    	$get = DB::table('tbllibro')
            ->join('tbllibrostock','tbllibrostock.idLibro','=','tbllibro.idLibro')
            ->where('tbllibro.estado',1)
    		->get()
            ->all();

    	return $get;
    }
	 /**
     * Método que actualiza el libro.
     *
     * @param $request, idLibro
     * @return array => stdArray
     * @author Victor Curilao
     **/  
    public function updateLibro($request,$idLibro){

        $update = DB::table('tbllibro')
                    ->where('idLibro',$idLibro)
                    ->update(array(
                        'nombre' => $request->nombreLibro,
                        'autor' => $request->nombreAutor,
                        'detalle'=> $request->detalleLibro
                    ));

        return $update;
    }


     /**
     * Método que inserta un nuevo libro
     *
     * @param $request
     * @return array => stdArray
     * @author Victor Curilao
     **/
    public function createLibro($request){

        $insert = DB::table('tbllibro')
                    ->insertGetId(array(
                        'nombre' => $request->nombreLibro,
                        'autor' => $request->nombreAutor,
                        'detalle'=> $request->detalleLibro
                    ));

        return $insert;
    }

    /**
     * Método que busca la lista de libros actual.
     *
     * @param $request
     * @return array => stdArray
     * @author Victor Curilao
     **/  
    public function insertStock($idLibro,$cantidad){

        $get = DB::table('tbllibrostock')
                     ->insert(array(
                        'idLibro' => $idLibro,
                        'cantidad' => $cantidad
                    ));

        return $get;
    }

    /**
     * Método que busca el stock de los libros para actualizarlos.
     *
     * @param $request
     * @return array => stdArray
     * @author Victor Curilao
     **/  
    public function updateStockByLibro($idLibro,$cantidad){

        $get = DB::table('tbllibrostock')
                    ->where('idLibro',$idLibro)
                     ->update(array(
                        'cantidad' => $cantidad
                    ));

        return $get;
    }


    /**
     * Método que busca la lista de libros actual.
     *
     * @param $request
     * @return array => stdArray
     * @author Victor Curilao
     **/  
    public function deleteLibro($idLibro){

        $get = DB::table('tbllibro')
                    ->where('idLibro',$idLibro)
                     ->update(array(
                        'estado' => 0
                    ));
        return $get;
    }
}
