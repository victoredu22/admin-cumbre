<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class AlumnoxCurso extends Model
{
    public function getCursoById($idCurso){

   		$get = DB::table('tblalumnoxcurso')
   			->join('tblcurso','tblalumnoxcurso.idCurso','=','tblcurso.idCurso')
   			->join('tblusuario','tblalumnoxcurso.idUsuario','=','tblusuario.idUsuario')
   			->join('tbldetalleusuario','tblusuario.idUsuario','=','tbldetalleusuario.idUsuario')
    		->where('tblalumnoxcurso.idCurso',$idCurso)
    		->get()
    		->all();

    	return $get;
   	}
}
