<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('index', function () {
    return view('index');
});


Route::any('/index', array(
	'as' => 'index',
	'uses' => 'FrontController@index'
));

Route::any('/createLibro', array(
	'as' => 'createLibro',
	'uses' => 'LibrosController@createLibro'
));
Route::any('/updateLibro/{idLibro}', array(
	'as' => 'updateLibro',
	'uses' => 'LibrosController@updateLibro'
));
Route::any('/deleteLibro/{idLibro}', array(
	'as' => 'deleteLibro',
	'uses' => 'LibrosController@deleteLibro'
));

Route::any('/admin-biblioteca', array(
	'as' => 'admin-biblioteca',
	'uses' => 'BibliotecaController@adminLibros'
));
Route::any('/librosAll', array(
	'as' => 'librosAll',
	'uses' => 'LibrosController@librosAll'
));


Route::any('/libros', array(
	'as' => 'libros',
	'uses' => 'BibliotecaController@librosIndex'
));

Route::any('/cursos', array(
	'as' => 'cursos',
	'uses' => 'CursoController@getCursos'
));

Route::any('/alumnoxcurso/{idCurso}', array(
	'as' => 'alumnoxcurso',
	'uses' => 'AlumnoxCursoController@getCursoById'
));

Route::any('/createReserva', array(
	'as' => 'createReserva',
	'uses' => 'CursoController@insertReserva'
));